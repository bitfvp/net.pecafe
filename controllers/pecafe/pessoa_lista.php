<?php
function fnccadastrolist(){
    $sql = "SELECT * FROM pecafe_cadastros ORDER BY nome";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cadastrolista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $cadastrolista;
}

function fncgetcadastro($id){
    $sql = "SELECT * FROM pecafe_cadastros WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getpessoa = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getpessoa;
}