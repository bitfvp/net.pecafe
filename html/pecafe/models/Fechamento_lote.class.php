<?php
class Fechamento_lote{
    public function fncfechamento_loteinsert($id_fechamento, $descricao, $sacas, $peso, $preco, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO pecafe_fechamentos_lotes ";
            $sql.="(id, id_fechamento, descricao, sacas, peso, preco, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :id_fechamento, :descricao, :sacas, :peso, :preco, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":id_fechamento", $id_fechamento);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":sacas", $sacas);
            $insere->bindValue(":peso", $peso);
            $insere->bindValue(":preco", $preco);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            header("Location: index.php?pg=Vfechamento&id={$id_fechamento}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncfechamento_loteupdate($id, $id_fechamento, $descricao, $sacas, $peso, $preco ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="pecafe_fechamentos_lotes ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE pecafe_fechamentos_lotes ";
                $sql.="SET ";
                $sql .= " descricao=:descricao,
sacas=:sacas,
peso=:peso,
preco=:preco
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":sacas", $sacas);
                $atualiza->bindValue(":peso", $peso);
                $atualiza->bindValue(":preco", $preco);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vfechamento&id={$id_fechamento}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncfechamento_lotedelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="pecafe_fechamentos_lotes ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE pecafe_fechamentos_lotes ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe