<?php
class Fechamento{
    public function fncfechamentoinsert( $corretagem_c, $corretagem_v, $ordem_compra, $comprador, $vendedor, $descarga, $descricao, $condicao_pag, $forma_pag, $condicao_entrega, $prazo_entrega, $observacao, $corretor, $usuario ){

//inserção no banco
        try{
            $sql="INSERT INTO pecafe_fechamentos ";
            $sql.="(id, corretagem_c, corretagem_v, ordem_compra, comprador, vendedor, descarga, descricao, condicao_pag, forma_pag, condicao_entrega, prazo_entrega, observacao, corretor, usuario)";
            $sql.=" VALUES ";
            $sql.="(NULL, :corretagem_c, :corretagem_v, :ordem_compra, :comprador, :vendedor, :descarga, :descricao, :condicao_pag, :forma_pag, :condicao_entrega, :prazo_entrega, :observacao, :corretor, :usuario )";
            global $pdo;
            $insere=$pdo->prepare($sql);
            $insere->bindValue(":corretagem_c", $corretagem_c);
            $insere->bindValue(":corretagem_v", $corretagem_v);
            $insere->bindValue(":ordem_compra", $ordem_compra);
            $insere->bindValue(":comprador", $comprador);
            $insere->bindValue(":vendedor", $vendedor);
            $insere->bindValue(":descarga", $descarga);
            $insere->bindValue(":descricao", $descricao);
            $insere->bindValue(":condicao_pag", $condicao_pag);
            $insere->bindValue(":forma_pag", $forma_pag);
            $insere->bindValue(":condicao_entrega", $condicao_entrega);
            $insere->bindValue(":prazo_entrega", $prazo_entrega);
            $insere->bindValue(":observacao", $observacao);
            $insere->bindValue(":corretor", $corretor);
            $insere->bindValue(":usuario", $usuario);
            $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erro'. $error_msg->getMessage();
        }

        if(isset($insere)){
/////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

            $sql = "SELECT Max(id) FROM ";
            $sql.="pecafe_fechamentos";
            global $pdo;
            $consulta = $pdo->prepare($sql);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
            $mid = $consulta->fetch();
            $sql=null;
            $consulta=null;

            $maid=$mid[0];

            header("Location: index.php?pg=Vfechamento&id={$maid}");
            exit();

        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncfechamentoupdate($id, $corretagem_c, $corretagem_v, $ordem_compra, $comprador, $vendedor, $descarga, $descricao, $condicao_pag, $forma_pag, $condicao_entrega, $prazo_entrega, $observacao, $corretor ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="pecafe_fechamentos ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE pecafe_fechamentos ";
                $sql.="SET ";
                $sql .= "corretagem_c=:corretagem_c,
corretagem_v=:corretagem_v,
ordem_compra=:ordem_compra,
comprador=:comprador,
vendedor=:vendedor,
descarga=:descarga,
descricao=:descricao,
condicao_pag=:condicao_pag,
forma_pag=:forma_pag,
condicao_entrega=:condicao_entrega,
prazo_entrega=:prazo_entrega,
observacao=:observacao,
corretor=:corretor
WHERE id=:id ";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":corretagem_c", $corretagem_c);
                $atualiza->bindValue(":corretagem_v", $corretagem_v);
                $atualiza->bindValue(":ordem_compra", $ordem_compra);
                $atualiza->bindValue(":comprador", $comprador);
                $atualiza->bindValue(":vendedor", $vendedor);
                $atualiza->bindValue(":descarga", $descarga);
                $atualiza->bindValue(":descricao", $descricao);
                $atualiza->bindValue(":condicao_pag", $condicao_pag);
                $atualiza->bindValue(":forma_pag", $forma_pag);
                $atualiza->bindValue(":condicao_entrega", $condicao_entrega);
                $atualiza->bindValue(":prazo_entrega", $prazo_entrega);
                $atualiza->bindValue(":observacao", $observacao);
                $atualiza->bindValue(":corretor", $corretor);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=Vfechamento&id={$id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

////////////////////////////////////////////////////
    public function fncfechamentodelete($tabela_id,$usuario_off ){
//verifica se existe
        try{
            $sql="SELECT 'id' FROM ";
            $sql.="pecafe_fechamentos ";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $tabela_id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarid=$consulta->rowCount();
        if($contarid!=0){
//comecar o update
            try {
                $sql="UPDATE pecafe_fechamentos ";
                $sql.="SET ";
                $sql .= "status=0,
usuario_off=:usuario_off,
data_off=CURRENT_TIMESTAMP
WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":usuario_off", $usuario_off);
                $atualiza->bindValue(":id", $tabela_id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);
            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
//msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há esse registro cadastrado em nosso sistema!!",
                "type"=>"warning",
            ];
        }//fim do if de contar
        if(isset($atualiza)){
//criar log
//reservado para log
            $_SESSION['fsh']=[
                "flash"=>"Desativada Com Sucesso!",
                "type"=>"success",
            ];
            header("Location: index.php?pg=V{pagina}&id={id}");
            exit();
        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
    }//fim da funcao

}//fim da classe