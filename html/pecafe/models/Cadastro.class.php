<?php
class cadastro{
    public function fnccadastroedit(
        $id,$nome,$cpf,$rg,$cnpj,$ie,$tipo_pessoa,$endereco,$numero,$bairro,$cidade,$complemento,$telefone,$email,$p_comprador,$p_vendedor
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
                $sql.="pecafe_cadastros";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                $sql="UPDATE pecafe_cadastros";
                $sql.=" SET";
                $sql .= " nome=:nome,
                    cpf=:cpf,
                    rg=:rg,
                    cnpj=:cnpj,
                    ie=:ie,
                    tipo_pessoa=:tipo_pessoa,
                    endereco=:endereco,
                    numero=:numero,
                    bairro=:bairro,
                    cidade=:cidade,
                    complemento=:complemento,
                    telefone=:telefone,
                    email=:email,
                    p_comprador=:p_comprador,
                    p_vendedor=:p_vendedor
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":cnpj", $cnpj);
                $atualiza->bindValue(":ie", $ie);
                $atualiza->bindValue(":tipo_pessoa", $tipo_pessoa);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":cidade", $cidade);
                $atualiza->bindValue(":complemento", $complemento);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":email", $email);
                $atualiza->bindValue(":p_comprador", $p_comprador);
                $atualiza->bindValue(":p_vendedor", $p_vendedor);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vcadastro&id={$id}");
                exit();


        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccadastronew(
        $nome,$cpf,$rg,$cnpj,$ie,$tipo_pessoa,$endereco,$numero,$bairro,$cidade,$complemento,$telefone,$email,$p_comprador,$p_vendedor
    ){
        //tratamento das variaveis
        //não ter

        try{
            $sql="SELECT id FROM ";
                $sql.="pecafe_cadastros";
            $sql.=" WHERE cpf=:cpf";
            global $pdo;
            $consultacpf=$pdo->prepare($sql);
            $consultacpf->bindValue(":cpf", $cpf);
            $consultacpf->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcpf=$consultacpf->rowCount();
        try{
            $sql="SELECT id FROM ";
                $sql.="pecafe_cadastros";
            $sql.=" WHERE cnpj=:cnpj";
            global $pdo;
            $consultacnpj=$pdo->prepare($sql);
            $consultacnpj->bindValue(":cnpj", $cnpj);
            $consultacnpj->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contarcnpj=$consultacnpj->rowCount();

        if(($contarcnpj==0)or ($cnpj=="")){
        if(($contarcpf==0)or ($cpf=="")){


            //inserção no banco
                try {
                    $sql="INSERT INTO pecafe_cadastros ";
                    $sql .= "(id,
                    nome,
                    cpf,
                    rg,
                    cnpj,
                    ie,
                    tipo_pessoa,
                    endereco,
                    numero,
                    bairro,
                    cidade,
                    complemento,
                    telefone,
                    email,
                    p_comprador,
                    p_vendedor
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :nome,
                    :cpf,
                    :rg,
                    :cnpj,
                    :ie,
                    :tipo_pessoa,
                    :endereco,
                    :numero,
                    :bairro,
                    :cidade,
                    :complemento,
                    :telefone,
                    :email,
                    :p_comprador,
                    :p_vendedor
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":nome", $nome);
                    $insere->bindValue(":cpf", $cpf);
                    $insere->bindValue(":rg", $rg);
                    $insere->bindValue(":cnpj", $cnpj);
                    $insere->bindValue(":ie", $ie);
                    $insere->bindValue(":tipo_pessoa", $tipo_pessoa);
                    $insere->bindValue(":endereco", $endereco);
                    $insere->bindValue(":numero", $numero);
                    $insere->bindValue(":bairro", $bairro);
                    $insere->bindValue(":cidade", $cidade);
                    $insere->bindValue(":complemento", $complemento);
                    $insere->bindValue(":telefone", $telefone);
                    $insere->bindValue(":email", $email);
                    $insere->bindValue(":p_comprador", $p_comprador);
                    $insere->bindValue(":p_vendedor", $p_vendedor);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }


        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse CPF!!",
                "type"=>"warning",
            ];
        }

        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, já ha uma pessoa cadastrada com esse CNPJ!!",
                "type"=>"warning",
            ];
        }


        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];

                $sql = "SELECT Max(id) FROM ";
                $sql.="pecafe_cadastros";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $mid = $consulta->fetch();
                $sql=null;
                $consulta=null;

                $maid=$mid[0];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

//                    header("Location: index.php?pg=Vcadastro&id={$maid}");
//                    exit();
            header("Location: index.php?pg=Vcadastro_lista");
            exit();


        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }




}
