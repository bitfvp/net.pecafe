<?php
class Conta{
    public function fnccontaedit(
        $id,$pessoa,$banco,$agencia,$conta,$cidade
    ){
        //tratamento das variaveis
        //não há
        try{
            $sql="SELECT * FROM ";
                $sql.="pecafe_cadastros";
            $sql.=" WHERE id=:id";
            global $pdo;
            $consulta=$pdo->prepare($sql);
            $consulta->bindValue(":id", $id);
            $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
        }catch ( PDOException $error_msg){
            echo 'Erroff'. $error_msg->getMessage();
        }
        $contar=$consulta->rowCount();
        if($contar!=0){
            //inserção no banco

            try {
                $sql="UPDATE pecafe_cadastros";
                $sql.=" SET";
                $sql .= " nome=:nome,
                    cpf=:cpf,
                    rg=:rg,
                    cnpj=:cnpj,
                    ie=:ie,
                    tipo_pessoa=:tipo_pessoa,
                    endereco=:endereco,
                    numero=:numero,
                    bairro=:bairro,
                    cidade=:cidade,
                    complemento=:complemento,
                    telefone=:telefone,
                    email=:email,
                    p_comprador=:p_comprador,
                    p_vendedor=:p_vendedor
                WHERE id=:id";
                global $pdo;
                $atualiza = $pdo->prepare($sql);
                $atualiza->bindValue(":nome", $nome);
                $atualiza->bindValue(":cpf", $cpf);
                $atualiza->bindValue(":rg", $rg);
                $atualiza->bindValue(":cnpj", $cnpj);
                $atualiza->bindValue(":ie", $ie);
                $atualiza->bindValue(":tipo_pessoa", $tipo_pessoa);
                $atualiza->bindValue(":endereco", $endereco);
                $atualiza->bindValue(":numero", $numero);
                $atualiza->bindValue(":bairro", $bairro);
                $atualiza->bindValue(":cidade", $cidade);
                $atualiza->bindValue(":complemento", $complemento);
                $atualiza->bindValue(":telefone", $telefone);
                $atualiza->bindValue(":email", $email);
                $atualiza->bindValue(":p_comprador", $p_comprador);
                $atualiza->bindValue(":p_vendedor", $p_vendedor);
                $atualiza->bindValue(":id", $id);
                $atualiza->execute(); global $LQ; $LQ->fnclogquery($sql);

            } catch (PDOException $error_msg) {
                echo 'Erro' . $error_msg->getMessage();
            }
        }else{
            //msg de erro para o usuario
            $_SESSION['fsh']=[
                "flash"=>"Ops, nao há essa pessoa cadastrada em nosso sistema!!",
                "type"=>"warning",
            ];

        }
        if(isset($atualiza)){
            /////////////////////////////////////////////////////
            //criar log
//            global $LL; $LL->fnclog($id,$_SESSION['id'],"Edição de pessoas",1,3);
            //reservado para log
            ////////////////////////////////////////////////////////////////////////////
            $_SESSION['fsh']=[
                "flash"=>"Atualização de Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
                header("Location: index.php?pg=Vcadastro&id={$id}");
                exit();


        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];

            }
        }
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function fnccontanew(
        $pessoa,$banco,$agencia,$conta,$cidade
    ){
        //tratamento das variaveis
        //não ter

            //inserção no banco
                try {
                    $sql="INSERT INTO pecafe_contas ";
                    $sql .= "(id,
                    pessoa,
                    banco,
                    agencia,
                    conta,
                    cidade
                    )";
                    $sql .= " VALUES ";
                    $sql .= "(NULL,
                    :pessoa,
                    :banco,
                    :agencia,
                    :conta,
                    :cidade
                    )";
                    global $pdo;
                    $insere = $pdo->prepare($sql);
                    $insere->bindValue(":pessoa", $pessoa);
                    $insere->bindValue(":banco", $banco);
                    $insere->bindValue(":agencia", $agencia);
                    $insere->bindValue(":conta", $conta);
                    $insere->bindValue(":cidade", $cidade);
                    $insere->execute(); global $LQ; $LQ->fnclogquery($sql);
                } catch (PDOException $error_msg) {
                    echo 'Erro' . $error_msg->getMessage();
                }



        if(isset($insere)){

            $_SESSION['fsh']=[
                "flash"=>"Cadastro realizado com sucesso!!",
                "type"=>"success",
            ];
            /////////////////////////////////////////////////////
            //reservado para log
//            global $LL; $LL->fnclog($maid,$_SESSION['id'],"Nova pessoa",1,1);
            ////////////////////////////////////////////////////////////////////////////

                    header("Location: index.php?pg=Vcadastro&id={$pessoa}");
                    exit();



        }else{
            if(!isset($_SESSION['fsh']) or $_SESSION['fsh']==null){
                $_SESSION['fsh']=[
                    "flash"=>"Ops!houve algo errado no nosso sistema, contate um administrador",
                    "type"=>"danger",
                ];
            }
        }
        
    }




}
