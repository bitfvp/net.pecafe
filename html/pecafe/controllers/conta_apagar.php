<?php
//metodo
if ($startactiona == 1 && $aca == "contadelete") {

    if (isset($_GET['id']) and is_numeric($_GET['id'])) {
        $cadastro = $_GET['id'];
    } else {
        $_SESSION['fsh'] = [
            "flash" => "Houve um erro!!, não encontrado o id da cadastro",
            "type" => "warning",
        ];
        header("Location: {$env->env_url_mod}index.php?pg=Vhome");
        exit();
    }

    if (isset($_GET['idconta']) and is_numeric($_GET['idconta']) and $_GET['idconta']>0){
        $conta = $_GET['idconta'];
    }else{
        $_SESSION['fsh'] = [
            "flash" => "Houve um erro!!, não encontrado o id da conta",
            "type" => "warning",
        ];
        header("Location: {$env->env_url_mod}index.php?pg=Vhome");
        exit();
    }


    try {
        $sql = "DELETE FROM `pecafe_contas` WHERE id = :id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $conta);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh'] = [
        "flash" => "Conta apagada com sucesso!!",
        "type" => "success",
    ];
    header("Location: index.php?pg=Vcadastro&id={$cadastro}");
    exit();

}

