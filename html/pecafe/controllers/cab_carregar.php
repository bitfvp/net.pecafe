<?php

function fnccablist(){
    $sql = "SELECT * FROM pecafe_cabecalhos ORDER BY empresa";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $cablista = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
    return $cablista;
}


function fncgetcab($id){
    $sql = "SELECT * FROM pecafe_cabecalhos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1,$id);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $getcab = $consulta->fetch();
    $sql=null;
    $consulta=null;
    return $getcab;
}
