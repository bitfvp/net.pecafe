<?php
//gerado pelo geracode
function fncfechamentolist(){
    $sql = "SELECT * FROM pecafe_fechamentos ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $fechamentolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $fechamentolista;
}

function fncgetfechamento($id){
    $sql = "SELECT * FROM pecafe_fechamentos WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getpecafe_fechamentos = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getpecafe_fechamentos;
}
?>