<?php
//gerado pelo geracode
function fncobservacaolist(){
    $sql = "SELECT * FROM pecafe_observacoes ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $observacaolista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $observacaolista;
}

function fncgetobservacao($id){
    $sql = "SELECT * FROM pecafe_observacoes WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getpecafe_observacoes = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getpecafe_observacoes;
}
?>