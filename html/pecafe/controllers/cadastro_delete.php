<?php
//metodo
if ($startactiona == 1 && $aca == "apagarcadastro") {

    if (isset($_GET['id']) and is_numeric($_GET['id'])) {
        $id = $_GET['id'];
    } else {
        $_SESSION['fsh'] = [
            "flash" => "Houve um erro!!, não encontrado o id da cadastro",
            "type" => "warning",
        ];
        header("Location: {$env->env_url_mod}index.php?pg=Vcadastro_lista");
        exit();
    }

    try {
        $sql = "DELETE FROM `pecafe_cadastros` WHERE id = :id ";
        global $pdo;
        $exclui = $pdo->prepare($sql);
        $exclui->bindValue(":id", $id);
        $exclui->execute();
    } catch (PDOException $error_msg) {
        echo 'Erro:' . $error_msg->getMessage();
    }

    $_SESSION['fsh'] = [
        "flash" => "cadastro apagada com sucesso!!",
        "type" => "success",
    ];
    header("Location: index.php?pg=Vcadastro_lista");
    exit();

}

