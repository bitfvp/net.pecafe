<?php
//gerado pelo geracode
function fncfechamento_lotelist(){
    $sql = "SELECT * FROM pecafe_fechamentos_lotes ORDER BY id";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $fechamento_lotelista = $consulta->fetchAll();
    $sql = null;
    $consulta = null;
    return $fechamento_lotelista;
}

function fncgetfechamento_lote($id){
    $sql = "SELECT * FROM pecafe_fechamentos_lotes WHERE id=?";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindParam(1, $id);
    $consulta->execute();
    global $LQ;
    $LQ->fnclogquery($sql);
    $getpecafe_fechamentos_lotes = $consulta->fetch();
    $sql = null;
    $consulta = null;
    return $getpecafe_fechamentos_lotes;
}
?>