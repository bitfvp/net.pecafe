<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Editar pessoa-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="cadastrosave";
    $cadastro=fncgetcadastro($_GET['id']);


//    if ($pessoa['p_comprador']==1){
//        $p_comprador="checked";
//        $p_compradorn="";
//    }else{
//        $p_comprador="";
//        $p_compradorn="checked";
//    }
//    if ($pessoa['p_vendedor']==1){
//        $p_vendedor="checked";
//        $p_vendedorn="";
//    }else{
//        $p_vendedor="";
//        $p_vendedorn="checked";
//    }

}else{
    $a="cadastronew";

//    $p_comprador="checked";
//    $p_compradorn="";
//    $p_vendedor="checked";
//    $p_vendedorn="";

}
?>
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vcadastro_editar&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Cadastro</h3>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $cadastro['id']; ?>"/>
                <label for="nome">NOME</label>
                <input autocomplete="off" autofocus id="nome" placeholder="Nome completo" type="text" class="form-control" name="nome" value="<?php echo $cadastro['nome']; ?>" required/>
            </div>
            <div class="col-md-3">
                <label for="cpf">CPF</label>
                <input autocomplete="off" id="cpf" type="text" class="form-control" name="cpf" value="<?php echo $cadastro['cpf']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-3">
                <label for="rg">RG</label>
                <input autocomplete="off" id="rg" type="text" class="form-control" name="rg" value="<?php echo $cadastro['rg']; ?>" maxlength="29"/>
            </div>
            <div class="col-md-3">
                <label for="cnpj">CNPJ</label>
                <input autocomplete="off" id="cnpj" type="text" class="form-control" name="cnpj" value="<?php echo $cadastro['cnpj']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#cnpj').mask('00.000.000/0000-00', {reverse: true});
                    });
                </script>
            </div>
            <div class="col-md-3">
                <label for="ie">INSCRIÇÃO ESTADUAL</label>
                <input autocomplete="off" id="ie" type="text" class="form-control" name="ie" value="<?php echo $cadastro['ie']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#ie').mask('000000000.00-00', {reverse: true});
                    });
                </script>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <label for="endereco">ENDEREÇO</label>
                <input autocomplete="off" id="endereco" type="text" class="form-control" name="endereco" value="<?php echo $cadastro['endereco']; ?>"/>
            </div>
            <div class="col-md-2">
                <label for="numero">NÚMERO</label>
                <input id="numero" type="number" autocomplete="off" class="form-control" name="numero" value="<?php echo $cadastro['numero']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="bairro">BAIRRO</label>
                <input autocomplete="off" id="bairro" type="text" class="form-control" name="bairro" value="<?php echo $cadastro['bairro']; ?>"/>
            </div>
            <div class="col-md-4">
                <label for="cidade">CIDADE</label>
                <input autocomplete="off" id="cidade" type="text" class="form-control" name="cidade" value="<?php echo $cadastro['cidade']; ?>"/>
            </div>
            <div class="col-md-8">
                <label for="complemento">COMPLEMENTO</label>
                <input autocomplete="off" id="complemento" type="text" class="form-control" name="complemento" value="<?php echo $cadastro['complemento']; ?>"/>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <label for="telefone">TELEFONE</label>
                <input autocomplete="off" id="telefone" type="tel" class="form-control" name="telefone" value="<?php echo $cadastro['telefone']; ?>"/>
                <script>
                    $(document).ready(function(){
                        $('#telefone').mask('(00)00000-0000--(00)00000-0000', {reverse: false});
                    });
                </script>
            </div>
            <div class="col-md-5">
                <label for="email">E-MAIL</label>
                <input autocomplete="off" id="email" type="text" class="form-control" name="email" value="<?php echo $cadastro['email']; ?>"/>
            </div>
        </div>

        <style type="text/css">
            input[type=radio]{
                transform:scale(2.1);
            }

            .form-check label:last-child {
                transform:scale(1.5);
            }
        </style>

<!--        <div class="row py-2">-->
<!--            <div class="col-md-2">-->
<!--                <label for="p_comprador">COMPRADOR</label>-->
<!--                <div class="form-check mb-3">-->
<!--                    <input class="form-check-input" type="radio" name="p_comprador" id="p_comprador" value="1" --><?php //// echo $p_comprador; ?><!-- >-->
<!--                    <label class="form-check-label ml-3" for="p_comprador">-->
<!--                        SIM-->
<!--                    </label>-->
<!--                </div>-->
<!--                <div class="form-check">-->
<!--                    <input class="form-check-input" type="radio" name="p_comprador" id="p_comprador" value="0" --><?php ////echo $p_compradorn; ?><!-- >-->
<!--                    <label class="form-check-label ml-3" for="p_comprador">-->
<!--                        NÃO-->
<!--                    </label>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-md-2">-->
<!--                <label for="p_vendedor">VENDEDOR/PRODUTOR</label>-->
<!--                <div class="form-check mb-3">-->
<!--                    <input class="form-check-input" type="radio" name="p_vendedor" id="p_vendedor" value="1" --><?php ////echo $p_vendedor; ?><!-- >-->
<!--                    <label class="form-check-label ml-3" for="p_vendedor">-->
<!--                        SIM-->
<!--                    </label>-->
<!--                </div>-->
<!--                <div class="form-check">-->
<!--                    <input class="form-check-input" type="radio" name="p_vendedor" id="p_vendedor" value="0" --><?php ////echo $p_vendedorn; ?><!-- >-->
<!--                    <label class="form-check-label ml-3" for="p_vendedor">-->
<!--                        NÃO-->
<!--                    </label>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <hr>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block my-2" value="SALVAR"/>
            </div>
            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>
        </div>
    </form>
    </div>
    </form>

</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>