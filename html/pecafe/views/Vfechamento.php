<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vf_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $fechamento=fncgetfechamento($_GET['id']);
}else{
    header("Location: {$env->env_url_mod}index.php?pg=Vfechamento_lista");
    exit();
}

?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg-info text-light">
                    Fechamento
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <header>
                                NEGOCIAÇÃO:
                                <strong class="text-info">
                                    <?php
                                    echo utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}")))."-".$fechamento['id']."<br>";
                                    if (strlen($fechamento['ordem_compra'])>0){
                                        echo "<p class='text-dark mb-1'>ORDEM DE COMPRA:".strtoupper($fechamento['ordem_compra'])."</p>";
                                    }
                                    echo datahoraBanco2data($fechamento['data_ts']);
                                    ?>
                                </strong>
                            </header>
                        </div>

                        <div class="col-md-6">
                            <a href="index.php?pg=Vfechamento_editar&id=<?php echo $_GET['id'] ?>" title="Editar entrada" class="btn btn-primary btn-block fas fa-pen">
                                <br>EDITAR FECHAMENTO
                            </a>
                        </div>
                    </div>

                    <h6>
                        VENDEDOR:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetcadastro($fechamento['vendedor'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                        COMPRADOR:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetcadastro($fechamento['comprador'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                        LOCAL DE DESCARGA:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetpessoa($fechamento['descarga'])['nome']); ?>&nbsp;&nbsp;</strong><br>
                        CORRETAGEM DE COMPRA:
                        <strong class="text-info text-uppercase"><?php if ($fechamento['corretagem_c']>0){echo $fechamento['corretagem_c']."%";}else{echo 0;} ; ?>&nbsp;&nbsp;</strong><br>
                        CORRETAGEM DE VENDA:
                        <strong class="text-info text-uppercase"><?php if ($fechamento['corretagem_v']>0){echo $fechamento['corretagem_v']."%";}else{echo 0;} ; ?>&nbsp;&nbsp;</strong><br>

                        <hr>

                        <?php include_once("includes/fechamento_form.php");?>


                        DESCRIÇÃO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['descricao']); ?>&nbsp;&nbsp;</strong><br>
                        CONDIÇÃO DE PAGAMENTO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_pag']); ?>&nbsp;&nbsp;</strong><br>
                        FORMA DE PAGAMENTO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['forma_pag']); ?>&nbsp;&nbsp;</strong><br>
                        CONDIÇÃO DE ENTREGA:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['condicao_entrega']); ?>&nbsp;&nbsp;</strong><br>
                        PRAZO DE ENTREGA:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['prazo_entrega']); ?>&nbsp;&nbsp;</strong><br>

                        CORRETOR:
                        <strong class="text-info text-uppercase"><?php echo strtoupper(fncgetcorretor($fechamento['corretor'])['corretor']); ?>&nbsp;&nbsp;</strong><br>
                        OBSERVAÇÃO:
                        <strong class="text-info text-uppercase"><?php echo strtoupper($fechamento['observacao']); ?>&nbsp;&nbsp;</strong><br>
                    </h6>

                </div>
            </div>

        </div>

        <div class="col-md-12">
                <div class="card mt-2">
                    <div class="card-header bg-info text-light">
                        Impressão de comprovante
                    </div>
                    <div class="card-body">
                        <form action="index.php" target="_blank" method="get" enctype="multipart/form-data" id="formx">
                            <div class="row">
                                <div class="col-md-12">
                                    <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $_GET['id']; ?>"/>
                                    <input id="pg" type="hidden" class="txt bradius" name="pg" value="Vfechamento_print1"/>
                                    <label for="cab">CABEÇALHO:</label>
                                    <select name="cab" id="cab" class="form-control input-sm" data-live-search="true">

                                        <option selected="" data-tokens="" value="0">
                                            PADRÃO
                                        </option>
                                        <?php
                                        foreach (fnccablist() as $item) {
                                            ?>
                                            <option data-tokens="<?php echo $item['empresa'];?>" value="<?php echo $item['id'];?>">
                                                <?php echo $item['empresa']; ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <input type="submit" name="gogo" id="gogo" class="btn btn-lg btn-success btn-block mt-2" value="GERAR"/>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>



        </div>


    </div>



</div>

<?php
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>