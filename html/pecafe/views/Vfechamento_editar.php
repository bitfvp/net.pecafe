<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
//        if ($allow["allow_9"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }//senao vai executar abaixo
    }
}

$page="Home-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
echo"<META HTTP-EQUIV=REFRESH CONTENT = '3000;URL={$env->env_url_mod}index.php?pg=Vf_lista'>";
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $a="fechamentoupdate";
    $fechamento=fncgetfechamento($_GET['id']);
}else{
    $a="fechamentoinsert";
}
?>
<!--/////////////////////////////////////////////////////-->
<script type="text/javascript">

    $(document).ready(function () {
        $('#serchcomprador input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultcomprador");
            if (inputValf.length) {
                $.get("includes/comprador.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultcomprador p", function () {
            var comprador = this.id;
            $.when(
                // $(this).parents("#serchcomprador").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchcomprador").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultcomprador").empty()

                $(this).parents("#serchcomprador").find('input[type="text"]').val($(this).text()),
                $('#comprador').val(comprador),
                $(this).parent(".resultcomprador").empty()

            ).then(
                // function() {
                //     // roda depois de acao1 e acao2 terminarem
                //     setTimeout(function() {
                //         //do something special
                //         $( "#gogo" ).click();
                //     }, 500);
                // }
                );
        });
    });


    $(document).ready(function () {
        $('#serchvendedor input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultvendedor");
            if (inputValf.length) {
                $.get("includes/vendedor.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultvendedor p", function () {
            var vendedor = this.id;
            $.when(
                // $(this).parents("#serchvendedor").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchvendedor").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultvendedor").empty()

                $(this).parents("#serchvendedor").find('input[type="text"]').val($(this).text()),
                $('#vendedor').val(vendedor),
                $(this).parent(".resultvendedor").empty()

            ).then(
                // function() {
                //     // roda depois de acao1 e acao2 terminarem
                //     setTimeout(function() {
                //         //do something special
                //         $( "#gogo" ).click();
                //     }, 500);
                // }
                );
        });
    });


    $(document).ready(function () {
        $('#serchdescarga input[type="text"]').on("keyup input", function () {
            /* Get input value on change */
            var inputValf = $(this).val();
            var resultDropdownf = $(this).siblings(".resultdescarga");
            if (inputValf.length) {
                $.get("includes/comprador.php", {term: inputValf}).done(function (data) {
                    // Display the returned data in browser
                    resultDropdownf.html(data);
                });
            } else {
                resultDropdownf.empty();
            }
        });

        // Set search input value on click of result item
        $(document).on("click", ".resultdescarga p", function () {
            var descarga = this.id;
            $.when(
                // $(this).parents("#serchdescarga").find('input[type="text"]').val($(this).text()),
                // $(this).parents("#serchdescarga").find('input[type="hidden"]').val($(this).text()),
                // $(this).parent(".resultdescarga").empty()

                $(this).parents("#serchdescarga").find('input[type="text"]').val($(this).text()),
                $('#descarga').val(descarga),
                $(this).parent(".resultdescarga").empty()

            ).then(
                // function() {
                //     // roda depois de acao1 e acao2 terminarem
                //     setTimeout(function() {
                //         //do something special
                //         $( "#gogo" ).click();
                //     }, 500);
                // }
                );
        });
    });


</script>
<!--/////////////////////////////////////////////////////-->
<div class="container"><!--todo conteudo-->
    <form class="form-signin" action="<?php echo "index.php?pg=Vfechamento_lista&aca={$a}"; ?>" method="post" id="formx">
        <h3 class="form-cadastro-heading">Fechamento</h3>
        <hr>
        <div class="row">

            <input id="id" type="hidden" class="txt bradius" name="id" value="<?php echo $fechamento['id']; ?>"/>

            <div class="col-md-12">
                <label for="ordem_compra">Ordem de compra:</label>
                <input autocomplete="off" autofocus id="ordem_compra" type="text" class="form-control" name="ordem_compra" value="<?php echo $fechamento['ordem_compra']; ?>"/>
            </div>


            <div class="col-md-6" id="serchcomprador">
                <label for="comprador">Comprador:</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_comprador_id=$fechamento['comprador'];
                        $v_comprador=fncgetcadastro($fechamento['comprador'])['nome'];

                    }else{
                        $v_comprador_id="";
                        $v_comprador="";
                    }
                    $c_comprador = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input  autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="c<?php echo $c_comprador;?>" id="c<?php echo $c_comprador;?>" value="<?php echo $v_comprador; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input id="comprador" autocomplete="false" type="hidden" class="form-control" name="comprador" value="<?php echo $v_comprador_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_comprador">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultcomprador"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_comprador").click(function(ev){
                            document.getElementById('comprador').value='';
                            document.getElementById('c<?php echo $c_comprador;?>').value='';
                        });
                    });
                </script>
            </div>


            <div class="col-md-6" id="serchvendedor">
                <label for="vendedor">Vendedor:</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_vendedor_id=$fechamento['vendedor'];
                        $v_vendedor=fncgetpessoa($fechamento['vendedor'])['nome'];

                    }else{
                        $v_vendedor_id="";
                        $v_vendedor="";
                    }
                    $c_vendedor = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="v<?php echo $c_vendedor;?>" id="v<?php echo $c_vendedor;?>" value="<?php echo $v_vendedor; ?>" placeholder="Click no resultado ao aparecer"  required />
                    <input id="vendedor" autocomplete="false" type="hidden" class="form-control" name="vendedor" value="<?php echo $v_vendedor_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_vendedor">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultvendedor"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_vendedor").click(function(ev){
                            document.getElementById('vendedor').value='';
                            document.getElementById('v<?php echo $c_vendedor;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-6" id="serchdescarga">
                <label for="descarga">Descarga:</label>
                <div class="input-group">
                    <?php
                    if (isset($_GET['id']) and is_numeric($_GET['id'])){
                        $v_descarga_id=$fechamento['descarga'];
                        $v_descarga=fncgetpessoa($fechamento['descarga'])['nome'];

                    }else{
                        $v_descarga_id="";
                        $v_descarga="";
                    }
                    $c_descarga = mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')); ?>
                    <input autocomplete="false" autocomplete="off" type="text" class="form-control" aria-autocomplete="none" name="d<?php echo $c_descarga;?>" id="d<?php echo $c_descarga;?>" value="<?php echo $v_descarga; ?>" placeholder="Click no resultado ao aparecer" />
                    <input id="descarga" autocomplete="false" type="hidden" class="form-control" name="descarga" value="<?php echo $v_descarga_id; ?>" />
                    <div class="input-group-append">
                        <span class="input-group-text" id="r_descarga">
                            <i class="fa fas fa-backspace "></i>
                        </span>
                    </div>
                    <div class="resultdescarga"></div>
                </div>
                <script>
                    $(document).ready(function(){
                        $("#r_descarga").click(function(ev){
                            document.getElementById('descarga').value='';
                            document.getElementById('d<?php echo $c_descarga;?>').value='';
                        });
                    });
                </script>
            </div>

            <div class="col-md-3" id="serchdescarga_comprador">
                <label for="descarga_comprador"></label>
                <div class="input-group">
                    <div class="form-check mt-3">
                        <input type="checkbox" class="form-check-input" id="descarga_comprador" name="descarga_comprador">
                        <label class="form-check-label" for="descarga_comprador">Mesmo do comprador</label>
                    </div>
                </div>
            </div>


            <div class="col-md-12">
                <label for="descricao">Descrição:</label>
                <textarea id="descricao" onkeyup="limite_textarea(this.value,1000,descricao,'cont')" maxlength="1000" class="form-control" rows="2" name="descricao" required><?php echo $fechamento['descricao']; ?></textarea>
                <span id="cont">1000</span>/1000
            </div>

            <div class="col-md-6">
                <label for="corretagem_c">Corretagem de compra:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">%</div>
                    </div>
                    <input id="corretagem_c" type="number" autocomplete="off" class="form-control" name="corretagem_c" value="<?php echo $fechamento['corretagem_c']; ?>" step="0.1" min="0" max="10" placeholder="use vírgula" required />
                </div>
            </div>

            <div class="col-md-6">
                <label for="corretagem_v">Corretagem de venda:</label>
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">%</div>
                    </div>
                    <input id="corretagem_v" type="number" autocomplete="off" class="form-control" name="corretagem_v" value="<?php echo $fechamento['corretagem_v']; ?>" step="0.1" min="0" max="10" placeholder="use vírgula" required />
                </div>

            </div>

            <div class="col-md-12">
                <label for="condicao_pag">Condição de pagamento:</label>
                <input autocomplete="off" id="condicao_pag" type="text" class="form-control" name="condicao_pag" value="<?php echo $fechamento['condicao_pag']; ?>"/>
            </div>

            <div class="col-md-12">
                <label for="forma_pag">Forma de pagamento: </label>
                <input autocomplete="off" spellcheck="false" id="forma_pag" type="text" class="form-control text-uppercase" name="forma_pag" value="<?php echo $fechamento['forma_pag']; ?>" placeholder=""/>
                <script>
                    function isererecebeu(valor) {
                        var str = $("#forma_pag").val();
                        $('#forma_pag').val(str+" "+valor);
                        // $('#quem_recebeu').focus();
                    }
                </script>

<!--                <div class="resultforma_pag"></div>-->
<!---->
<!--                --><?php
//                foreach (fnccontalist($fechamento['vendedor']) as $contas){
//                    echo "<i id='emoji' class='btn btn-sm m-1 border btn-outline-dark float-right' onclick=\"isererecebeu('".$contas['banco']." AG-".$contas['agencia']." C-".$contas['conta']." ".$contas['cidade']."; ')\">".$contas['banco']." AG-".$contas['agencia']." C-".$contas['conta']." ".$contas['cidade']."</i>";
//                }
//                ?>
            </div>

            <div class="col-md-4">
                <label for="condicao_entrega">Condição de entrega:</label>
                <input autocomplete="off" id="condicao_entrega" type="text" class="form-control" name="condicao_entrega" value="<?php echo $fechamento['condicao_entrega']; ?>"/>
            </div>

            <div class="col-md-4">
                <label for="prazo_entrega">Prazo de entrega:</label>
                <input autocomplete="off" id="prazo_entrega" type="text" class="form-control" name="prazo_entrega" value="<?php echo $fechamento['prazo_entrega']; ?>"/>
            </div>

            <div class="col-md-4">
                <label for="corretor">Corretor:</label>
                <select name="corretor" required="true" id="corretor" class="form-control">
                    // vamos criar a visualização
                    <option selected="" value="<?php echo $fechamento['corretor'];?>">
                        <?php
                        $corretor=fncgetcorretor($fechamento['corretor']);
                        echo $corretor['corretor'];
                        ?>
                    </option>
                    <?php
                    foreach(fnccorretorlist() as $item){?>
                        <option value="<?php echo $item['id']; ?>"><?php echo $item['corretor']; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-md-12">
                <label for="observacao">observação:</label>
                <textarea id="observacao" onkeyup="limite_textarea(this.value,1000,observacao,'cont2')" maxlength="1000" class="form-control" rows="2" name="observacao"><?php echo $fechamento['observacao']; ?></textarea>
                <span id="cont2">1000</span>/1000
            </div>


            <div class="col-md-12 pt-2" id="">
                <input type="submit" value="SALVAR" name="gogo" id="gogo" class="btn btn-block btn-success mt-4">
            </div>

            <script>
                var formID = document.getElementById("formx");
                var send = $("#gogo");

                $(formID).submit(function(event){
                    if (formID.checkValidity()) {
                        send.attr('disabled', 'disabled');
                        send.attr('value', 'AGUARDE...');
                    }
                });
            </script>


        </div>

    </form>
</div>

<?php
//for ($i = 1; $i <= 1000000; $i++) {
//    echo $i."  ".get_criptografa64($i)."<br>";
//}
include_once("{$env->env_root}includes/footer.php");
?>
</body>
</html>