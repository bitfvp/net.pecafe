<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de fechamentos por comprador-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";
$comprador=$_POST['comprador'];

if (isset($_POST['corretor']) and is_numeric($_POST['corretor']) and $_POST['corretor']!=0){
    $corretor=$_POST['corretor'];
    $sql = "SELECT * FROM "
        ."pecafe_fechamentos "
        ."WHERE "
        ."pecafe_fechamentos.comprador = :comprador "
        ."AND pecafe_fechamentos.corretor = :corretor "
        ."AND pecafe_fechamentos.`data_ts` >= :inicial "
        ."AND pecafe_fechamentos.`data_ts` <= :final "
        ."ORDER BY pecafe_fechamentos.data_ts ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":comprador",$comprador);
    $consulta->bindValue(":corretor",$corretor);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $fechamentos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}else{
    $sql = "SELECT * FROM "
        ."pecafe_fechamentos "
        ."WHERE "
        ."pecafe_fechamentos.comprador = :comprador "
        ."AND pecafe_fechamentos.`data_ts` >= :inicial "
        ."AND pecafe_fechamentos.`data_ts` <= :final "
        ."ORDER BY pecafe_fechamentos.data_ts ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":comprador",$comprador);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $fechamentos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}


?>
<div class="container">

    <?php
    if ($_GET['cab']==0){
        include_once("includes/pecafe_cab.php");
    }else{
        $cab=fncgetcab($_GET['cab']);
        echo "<div class='row'>";
        echo "<div class='col-10 offset-1 text-center'>";
        echo "<h3 class='mt-1'>".$cab['empresa']."</h3>";
        echo "<h6>".$cab['linha1']."</h6>";
        echo "<h6>".$cab['linha2']."</h6>";
        echo "</div>";
        echo "</div>";

    }
    ?>


    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-uppercase">RELATÓRIO DE FECHAMENTOS POR COMPRADOR</h3>
            <h5>
                <?php
                echo strtoupper(fncgetpessoa($_POST['comprador'])['nome']);
                ?>
            </h5>
            <?php
            if (isset($_POST['corretor']) and is_numeric($_POST['corretor']) and $_POST['corretor']!=0){
                echo "<h6>";
                echo strtoupper(fncgetcorretor($_POST['corretor'])['corretor']);
                echo "<h6>";
            }
            ?>
            <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
        </div>
    </div>


    <table class="table table-sm">
        <thead class="thead-dark">
            <tr>
                <td>ORD.COMPRA</td>
                <td>DATA</td>
                <td>VENDEDOR</td>
                <td>SACAS</td>
                <td>TOTAL</td>
                <td>PORC.</td>
                <td>COMISSÃO</td>
            </tr>
        </thead>

        <tbody>
        <?php
        $total_geral=0;
        $sacas_geral=0;
        $comissao_geral=0;
        foreach ($fechamentos as $fechamento){?>
            <tr>
                <td><?php echo strtoupper($fechamento['ordem_compra']); ?></td>
                <td><?php echo dataRetiraHora($fechamento['data_ts']); ?></td>
                <td>
                    <?php
                    $vendedor=fncgetpessoa($fechamento['vendedor']);
                    echo strtoupper($vendedor['nome']);
                    ?>
                </td>

                <?php

                try{
                    $sql="SELECT * FROM ";
                    $sql.="pecafe_fechamentos_lotes ";
                    $sql.="WHERE id_fechamento=:id and status=1";
                    global $pdo;
                    $consulta=$pdo->prepare($sql);
                    $consulta->bindValue(":id", $fechamento['id']);
                    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                    echo 'Erroff'. $error_msg->getMessage();
                }
                $lotes=$consulta->fetchAll();

                $sacas=0;
                $total=0;
                $comissao=0;
                foreach ($lotes as $dados){
                    $sacas=$sacas+$dados['sacas'];
                    $total=$total+($dados['sacas']*$dados['preco']);
                }
                ?>


                <td><?php echo $sacas; ?></td>
                <td>R$<?php echo number_format($total,2); ?></td>
                <td><?php echo $fechamento['corretagem_c']; ?>%</td>
                <td>
                    R$<?php
                    $comissao=($total/100)*$fechamento['corretagem_c'];
                    echo number_format($comissao,2);
                    ?>
                </td>
            </tr>


        <?php
            $total_geral=$total_geral+$total;
            $sacas_geral=$sacas_geral+$sacas;
            $comissao_geral=$comissao_geral+$comissao;
        } ?>

        <tr class="font-weight-bold">
            <td colspan="3"></td>
            <td><?php echo $sacas_geral; ?></td>
            <td>R$<?php echo number_format($total_geral,2); ?></td>
            <td></td>
            <td>
                R$<?php
                echo number_format($comissao_geral,2);
                ?>
            </td>
        </tr>
        </tbody>

    </table>





    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
        <div class="col-12">
            <strong> Concordo com os dados prescritos acima e dou fé.</strong>
        </div>
    </div>
    <br>
    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <h4>_______________________</h4>
            <h4>
                Assinatura
            </h4>
        </div>
    </div>


</div>
</html>