<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
        if ($allow["allow_5"]!=1){
            header("Location: {$env->env_url}?pg=Vlogin");
            exit();
        }
    }
}

$page="cadastro-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
if (isset($_GET['id']) and is_numeric($_GET['id'])){
    $cadastro=fncgetcadastro($_GET['id']);
}else{
    echo "Houve um erro, entre em contato com o suporte";
    exit();
}
?>
<main class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="container-fluid">
                <h3 class="ml-3">DADOS</h3>
                <blockquote class="blockquote blockquote-info">
                    <header>
                        NOME:
                        <strong class="text-info"><?php echo strtoupper($cadastro['nome']); ?>&nbsp;&nbsp;</strong>
                    </header>
                    <h6>
                        CPF:
                        <strong class="text-info"><?php
                            if($cadastro['cpf']!="" and $cadastro['cpf']!=0) {
                                echo "<span class='text-info'>";
                                echo mask($cadastro['cpf'],'###.###.###-##');
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        RG:
                        <strong class="text-info"><?php
                            if($cadastro['rg']!="" and $cadastro['rg']!=0) {
                                echo "<span class='text-info'>";
                                echo $cadastro['rg'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        CNPJ:
                        <strong class="text-info"><?php
                            if($cadastro['cnpj']!="" and $cadastro['cnpj']!=0) {
                                echo "<span class='text-info'>";
                                echo mask($cadastro['cnpj'],'##.###.###/####-##');
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        INSCRIÇÃO ESTADUAL:
                        <strong class="text-info"><?php
                            if($cadastro['ie']!="" and $cadastro['ie']!=0) {
                                echo "<span class='text-info'>";
                                echo mask($cadastro['ie'],'#########.##-##',);
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        <hr>

                        ENDEREÇO:
                        <strong class="text-info"><?php
                            if($cadastro['endereco']!=""){
                                echo "<span class='text-info  text-uppercase'>";
                                echo $cadastro['endereco'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>&nbsp;&nbsp;

                        NÚMERO:
                        <strong class="text-info"><?php
                            if ($cadastro['numero']==0){
                                echo "<span class='text-info'>";
                                echo "s/n";
                                echo "</span>";
                            }else{
                                echo "<span class='text-info'>";
                                echo $cadastro['numero'];
                                echo "</span>";
                            }
                            ?></strong>&nbsp;&nbsp;
                        <br>
                        BAIRRO:
                        <strong class="text-info"><?php
                            if($cadastro['bairro']!="") {
                                echo "<span class='text-info text-uppercase'>";
                                echo $cadastro['bairro'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;

                        CIDADE:
                        <strong class="text-info"><?php
                            if($cadastro['cidade']!="") {
                                echo "<span class='text-info text-uppercase'>";
                                echo strtoupper($cadastro['cidade']);
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;

                        COMPLEMENTO:
                        <strong class="text-info"><?php
                            if($cadastro['complemento']!="") {
                                echo "<span class='text-info'>";
                                echo $cadastro['complemento'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?>
                        </strong>&nbsp;

                        TELEFONE:
                        <strong class="text-info"><?php
                            if($cadastro['telefone']!="") {
                                echo "<span class='text-info'>";
                                echo $cadastro['telefone'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>

                        E-MAIL:
                        <strong class="text-info"><?php
                            if($cadastro['email']!="") {
                                echo "<span class='text-info'>";
                                echo $pessoa['email'];
                                echo "</span>";
                            }else{
                                echo "<span class='text-muted'>";
                                echo "[---]";
                                echo "</span>";
                            }
                            ?></strong>
                        <hr>

<?php
$perfil="";
if ($pessoa["p_comprador"]==1){
    $perfil .= "<i class='badge fas fa-briefcase text-info'>Comprador</i>";
}
if ($pessoa["p_vendedor"]==1){
    $perfil .= "<i class='badge fas fa-people-carry'>Vendedor/Produtor</i>";
}

?>&nbsp;&nbsp;

                        <strong>
                            <?php echo $perfil; ?>
                        </strong>
                    </h6>
                    <a class="btn btn-success btn-block" href="?pg=Vcadastro_editar&id=<?php echo $_GET['id']; ?>" title="Edite os dados desse cadastro">
                        EDITAR CADASTRO
                    </a>
                    <footer class="blockquote-footer">
                        Mantenha atualizado</strong>&nbsp;&nbsp;
                    </footer>

                </blockquote>


                <table class="table table-sm">
                    <thead class="thead-dark">
                    <tr>
                        <th>Banco</th>
                        <th>Agência</th>
                        <th>Conta</th>
                        <th>Cidade</th>
                        <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    try{
                        $sql="SELECT * FROM ";
                        $sql.="pecafe_contas";
                        $sql.=" WHERE pessoa=:id";
                        global $pdo;
                        $consulta=$pdo->prepare($sql);
                        $consulta->bindValue(":id", $_GET['id']);
                        $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                    }catch ( PDOException $error_msg){
                        echo 'Erroff'. $error_msg->getMessage();
                    }
                    $contas=$consulta->fetchAll();

                    foreach ($contas as $dados){?>
                        <tr>
                            <td><?php echo $dados['banco'];?></td>
                            <td><?php echo $dados['agencia'];?></td>
                            <td><?php echo $dados['conta'];?></td>
                            <td class="text-uppercase"><?php echo $dados['cidade'];?></td>
                            <td>
                                <div class="dropdown show">
                                    <a class="btn btn-danger btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-trash">EXCLUIR</i>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Não</a>
                                        <a class="dropdown-item bg-danger" href="index.php?pg=Vcadastro&id=<?php echo $_GET['id'];?>&aca=contadelete&idconta=<?php echo $dados['id'];?>">Apagar</a>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    <?php
                    }
                    ?>

                    <tr>
                        <form action="index.php?pg=Vcadastro&id=<?php echo $_GET['id'];?>&aca=contanew" method="post" id="formx">
                            <td><input type="text" name="banco" id="banco" class="form-control form-control-sm" value="" placeholder="Banco" ></td>
                            <td><input type="text" name="agencia" id="agencia" class="form-control form-control-sm" value="" placeholder="Agencia"></td>
                            <script>
                                $(document).ready(function(){
                                    $('#agencia').mask('000000', {reverse: false});
                                });
                            </script>
                            <td><input type="text" name="conta" id="conta" class="form-control form-control-sm" value="" placeholder="Conta"></td>
                            <script>
                                $(document).ready(function(){
                                    $('#conta').mask('00000-0', {reverse: true});
                                });
                            </script>
                            <td><input type="text" name="cidade" id="cidade" class="form-control form-control-sm" value="" placeholder="Cidade"></td>
                            <td>
                                <button type="submit" name="gogo" id="gogo" class="btn btn-sm btn-block btn-success">SALVAR</button>
                                <script>
                                    var formID = document.getElementById("formx");
                                    var send = $("#gogo");

                                    $(formID).submit(function(event){
                                        if (formID.checkValidity()) {
                                            send.attr('value', 'AGUARDE...');
                                            send.attr('disabled', 'disabled');
                                        }
                                    });
                                </script>
                            </td>
                        </form>
                    </tr>
                    </tbody>

                </table>

                <hr>


            </div>

        </div>
<!--        <div class="col-md-4">-->
<!--            <section class="sidebar-offcanvas" id="sidebar">-->
<!--                <div class="list-group">-->
<!--                    <a href="?pg=Vcadastro&id=--><?php //echo $_GET['id']; ?><!--" class="list-group-item"><i class="fas fa-home"></i>    PESSOA</a>-->
<!--                    <a href="?pg=Vcadastro_l&id=--><?php //echo $_GET['id']; ?><!--" class="list-group-item"><i class="fas fa-boxes"></i>    LOTES</a>-->
<!--                </div>-->
<!--            </section>-->
<!--            <script type="application/javascript">-->
<!--                var offset = $('#sidebar').offset().top;-->
<!--                var $meuMenu = $('#sidebar'); // guardar o elemento na memoria para melhorar performance-->
<!--                $(document).on('scroll', function () {-->
<!--                    if (offset <= $(window).scrollTop()) {-->
<!--                        $meuMenu.addClass('fixarmenu');-->
<!--                    } else {-->
<!--                        $meuMenu.removeClass('fixarmenu');-->
<!--                    }-->
<!--                });-->
<!--            </script>-->
<!--        </div>-->

    </div>
</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>