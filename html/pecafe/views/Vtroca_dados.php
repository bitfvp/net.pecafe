<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
        //validação das permissoes
        //if ()
    }
}
$page="Alterar Dados-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");

    $a="usuariosave2";
    $us_er=fncgetusuario($_SESSION['id']);
?>


<main class="container">

    <form class="frmgrid" action="index.php?pg=Vtroca_dados&aca=<?php echo $a;?>" method="post">
        <div class="row">
            <div class="col">
                <input type="submit" value="SALVAR" class="btn btn-success btn-block" />
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label  class="large" for="nome">Nome:</label>
                <input autocomplete="off" autofocus id="nome" type="text" class="form-control" name="nome" value="<?php echo $us_er['nome']; ?>" required/>
            </div>


            <div class="col-md-3">
                <label  class="large" for="">Nascimento:</label>
                <input name="nascimento" id="nascimento" type="date" class="form-control" value="<?php echo $us_er['nascimento']; ?>" required/>
            </div>


            <div class="col-md-3">
                <label  class="large" for="">CPF:</label>
                <input name="cpf" id="cpf" type="text" class="form-control" value="<?php echo $us_er['cpf']; ?>" required/>
                <script>
                    $(document).ready(function(){
                        $('#cpf').mask('000.000.000-00', {reverse: false});
                    });
                </script>
            </div>

        </div>

    </form>
</main>

<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>