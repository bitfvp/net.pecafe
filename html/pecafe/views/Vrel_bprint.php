<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{

    }
}

$page="Relatório de fechamentos por corretor-".$env->env_titulo;
$css="style1";
include_once("{$env->env_root}includes/head.php");

// Recebe
$inicial=$_POST['data_inicial'];
$inicial.=" 00:00:00";
$final=$_POST['data_final'];
$final.=" 23:59:59";

if (isset($_POST['corretor']) and is_numeric($_POST['corretor']) and $_POST['corretor']!=0){
    $corretor=$_POST['corretor'];
    $sql = "SELECT * FROM "
        ."pecafe_fechamentos "
        ."WHERE "
        ."pecafe_fechamentos.corretor = :corretor "
        ."AND pecafe_fechamentos.`data_ts` >= :inicial "
        ."AND pecafe_fechamentos.`data_ts` <= :final "
        ."ORDER BY pecafe_fechamentos.data_ts ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":corretor",$corretor);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $fechamentos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}else{
    $sql = "SELECT * FROM "
        ."pecafe_fechamentos "
        ."WHERE "
        ."pecafe_fechamentos.`data_ts` >= :inicial "
        ."AND pecafe_fechamentos.`data_ts` <= :final "
        ."ORDER BY pecafe_fechamentos.data_ts ASC ";
    global $pdo;
    $consulta = $pdo->prepare($sql);
    $consulta->bindValue(":inicial",$inicial);
    $consulta->bindValue(":final",$final);
    $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
    $fechamentos = $consulta->fetchAll();
    $sql=null;
    $consulta=null;
}


?>
<div class="container">

    <?php
        include_once("includes/pecafe_cab.php");
    ?>



    <div class="row">
        <div class="col-12 text-center">
            <h3 class="text-uppercase">RELATÓRIO DE FECHAMENTOS POR CORRETOR</h3>
            <?php
            if (isset($_POST['corretor']) and is_numeric($_POST['corretor']) and $_POST['corretor']!=0){
                $corretor=fncgetcorretor($_POST['corretor']);
                echo "<h6><strong>";
                echo strtoupper($corretor['corretor'])." / ".$corretor['comissao']."%";
                echo "</strong></h6>";
            }
            ?>
            <h5>Período:<?php echo dataBanco2data($_POST['data_inicial'])." à ".dataBanco2data($_POST['data_final']);?></h5>
        </div>
    </div>

    <table class="table table-sm">
        <thead CLASS="thead-dark">
        <tr>
            <th>NR</th>
            <th>DATA</th>
            <th>COMPRADOR</th>
            <th>VENDEDOR/PRODUTOR</th>
            <th>SACAS</th>
            <th>TOTAL</th>
            <th>COMISSÃO</th>
        </tr>
        </thead>

        <tbody>

        <?php
        $sacas_geral=0;
        $total_geral=0;
        $comissao_geral=0;
        foreach ($fechamentos as $fechamento){?>

            <tr>
                <td><?php echo utf8_encode(strftime('%Y', strtotime("{$fechamento['data_ts']}")))."-".$fechamento['id']; ?></td>
                <td><?php echo dataRetiraHora($fechamento['data_ts']); ?></td>
                <td>
                    <?php
                    $comprador=fncgetcadastro($fechamento['comprador']);
                    echo strtoupper($comprador['nome']);
                    ?>
                </td>
                <td>
                    <?php
                    $vendedor=fncgetcadastro($fechamento['vendedor']);
                    echo strtoupper($vendedor['nome']);
                    ?>
                </td>

                <?php
                try{
                $sql="SELECT * FROM ";
                $sql.="pecafe_fechamentos_lotes ";
                $sql.="WHERE id_fechamento=:id and status=1";
                global $pdo;
                $consulta=$pdo->prepare($sql);
                $consulta->bindValue(":id", $fechamento['id']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                }catch ( PDOException $error_msg){
                echo 'Erroff'. $error_msg->getMessage();
                }
                $lotes=$consulta->fetchAll();

                $sum=0;
                $sacas=0;
                $total=0;
                foreach ($lotes as $dados){
                    $sacas=$sacas+$dados['sacas'];
                    $total= $total+($dados['sacas']*$dados['preco']);
                }
                $comissao=($total/100)*$corretor['comissao'];
                ?>



                <td>
                    <?php echo $sacas; $sacas_geral=$sacas_geral+$sacas;?>
                </td>
                <td>
                    R$<?php echo number_format($total,2); $total_geral=$total_geral+$total; ?>
                </td>
                <td>
                    R$<?php echo number_format($comissao,2); $comissao_geral=$comissao_geral+$comissao; ?>
                </td>
            </tr>


        <?php } ?>

        <tr>
            <td colspan="4"></td>
            <td><?php echo $sacas_geral;?></td>
            <td>R$<?php echo number_format($total_geral,2); ?></td>
            <td>R$<?php echo number_format($comissao_geral,2); ?></td>
        </tr>

        </tbody>


    </table>




    <br>
    <br>
    <div class="row text-center">
        <div class="col-12">
            <?php echo date('d/m/Y')." ".date('H:i:s'); ?>
        </div>
    </div>
    <br>


</div>
</html>