<?php
if (!isset($_SESSION["logado"]) and $_SESSION["logado"]!="1") {
    //validação se esta logado
    header("Location: {$env->env_url}?pg=Vlogin");
    exit();
}else{
    if ($_SESSION["matriz"]!=1){
        //validação de matriz
        header("Location: {$env->env_url}?pg=Vlogin");
        exit();
    }else{
//        validação das permissoes
//        if ($allow["admin"]!=1){
//            header("Location: {$env->env_url}?pg=Vlogin");
//            exit();
//        }
    }
}


$page="Logs-".$env->env_titulo;
$css="style1";

include_once("{$env->env_root}includes/head.php");
include_once("includes/topo.php");
?>
<main class="container"><!--todo conteudo-->

    <div class="row">

        <div class="col-md-9">
            <!-- =============================começa conteudo======================================= -->
            <table class="table table-sm table-striped">
                <thead class="thead-default">
                <tr>
                    <th>PESSOA</th>
                    <th>DATA</th>
                    <th>DESCRIÇÂO</th>
                    <th>ATIVIDADE</th>
                    <th>TIPO</th>
                </tr>
                </thead>

                <tbody>
                <?php
                $sql = "SELECT pecafe_log.id, pecafe_log.pessoa AS codpessoa, pecafe_cadastros.nome AS pessoa, tbl_users.nome AS profissional, pecafe_log.data, pecafe_log.descricao, pecafe_loglista.atividade, pecafe_log.atividade_tipo\n"
                    . "FROM pecafe_cadastros INNER JOIN (tbl_users INNER JOIN (pecafe_loglista INNER JOIN pecafe_log ON pecafe_loglista.id = pecafe_log.atividade) ON tbl_users.id = pecafe_log.profissional) ON pecafe_cadastros.id = pecafe_log.pessoa\n"
                    . "WHERE (((pecafe_log.profissional)=?))\n"
                    . "ORDER BY pecafe_log.data DESC";
                global $pdo;
                $consulta = $pdo->prepare($sql);
                $consulta->bindParam(1, $_GET['id']);
                $consulta->execute(); global $LQ; $LQ->fnclogquery($sql);
                $logs = $consulta->fetchall();
                $logscount = $consulta->rowCount();
                $sql=null;
                $consulta=null;

                foreach ($logs as $log){
                    $tempdata=datahoraBanco2data($log['data']);
                    if ($log['atividade_tipo']==1){$temptipo="NOVO";}
                    if ($log['atividade_tipo']==2){$temptipo="READ";}
                    if ($log['atividade_tipo']==3){$temptipo="ATUALIZAÇÂO";}
                    if ($log['atividade_tipo']==4){$temptipo="EXCLUSÂO";}
                    echo "<tr>";
                    echo "<td>{$log['pessoa']}</td>";
                    echo "<td>{$tempdata}</td>";
                    echo "<td>{$log['descricao']}</td>";
                    echo "<td>{$log['atividade']}</td>";
                    echo "<td>{$temptipo}</td>";
                    echo "</tr>";
                } ?>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="5" class="text-right"><?php echo $logscount;?> Log(s) listado(s)</th>
                </tr>
                </tfoot>
            </table>
            <!-- =============================fim conteudo======================================= -->
        </div>
        <div class="col-md-3">
            <?php include_once("includes/sectionmenulateral.php"); ?>
        </div>
    </div>

</main><!--fim de conteiner-->
<?php include_once("{$env->env_root}includes/footer.php"); ?>
</body>
</html>