<?php
// adicione antes de chamar o footer
// $oculta_online=1;//variavel setada para ocultar contatos online se tiver logado
?>

<hr>

<footer class="footer">
    <div class="container text-center">
        <p class="">
            JA/F
            ® <?php echo date("Y"); ?>
        </p>
    </div>
</footer>

<?php
//se não quizer que apareceça os onlines set a variavel $oculta_online
if (!isset($oculta_online)){
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="1") {
        include_once("{$env->env_root}/includes/pecafe/online.php");
    }
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="2") {
        include_once("{$env->env_root}/includes/xxx/online.php");
    }
    if (isset($_SESSION["logado"]) and $_SESSION["logado"]=="1" and $_SESSION["matriz"]=="3") {
        include_once("{$env->env_root}/includes/yyy/online.php");
    }
}
?>